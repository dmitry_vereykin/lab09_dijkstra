//available at http://www.algolist.com/code/java/Dijkstra%27s_algorithm
//implements Dijkstra's Shortest Path Algorithm
 
import java.io.*;
import java.util.*;

///////////////
//Vertex class 
///////////////
class Vertex implements Comparable<Vertex>
{
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName) { name = argName; }
    public String toString() { return name; }
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }
}

///////////////
//Edge class 
///////////////
class Edge
{
    public final Vertex target;
    public final double weight;
    public Edge(Vertex argTarget, double argWeight)
    { target = argTarget; weight = argWeight; }
}


////////////////////////////////////
// Dijkstra's Shortest path class 
///////////////////////////////////
public class Dijkstra
{
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

	     while (!vertexQueue.isEmpty()) 
        {
	        Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
		          if (distanceThroughU < v.minDistance) 
                {
		               vertexQueue.remove(v);
		               v.minDistance = distanceThroughU;
		               v.previous = u;
		               vertexQueue.add(v);
		          }
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);
        Collections.reverse(path);
        return path;
    }

    public static void main(String[] str) throws IOException {
        System.out.print("In order to add new cities and distances, please edit txt files\n");
        System.out.println("Use the index of the city!");

        int from;
        int to;
        String enter;
        Scanner input = new Scanner(System.in);

        while (true) {
            FilesAndArrays far = new FilesAndArrays();
            far.readFileWithCities();
            far.readFileWithDistances();
            System.out.print("Departing from:  ");
            from = input.nextInt();
            System.out.print("Arriving to:  ");
            to = input.nextInt();

            if (from < far.numberOfCities) {
                computePaths(far.cities[from]);
                System.out.println("Distance from " + far.cities[from] + " to " + far.cities[to] + ": " + far.cities[to].minDistance);
                List<Vertex> path = getShortestPathTo(far.cities[to]);
                System.out.println("Path: " + path);
            }

            System.out.println("Press enter to continue or type \"stop\" to exit");
            input.nextLine();
            enter = input.nextLine();

            if (enter.equalsIgnoreCase("stop")) {
                System.out.println("Thanks for choosing MegaPremiumAirlines");
                break;
            }

        }


    }
 }//end Dijkstra class
