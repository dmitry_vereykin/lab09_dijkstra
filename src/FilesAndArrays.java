import java.io.*;
import java.util.Scanner;

/**
 * Created by Dmitry Vereykin on 11/17/2015.
 */
public class FilesAndArrays {

    public final int EXT = 100;
    public String[] namesOfCities = new String[EXT];
    public String[] distances = new String[EXT];
    public Vertex[] cities = new Vertex[EXT];
    public int numberOfCities = 0;
    public int numberOfDistances = 0;
    public Edge[] edges = new Edge[EXT];

    public void readFileWithCities() throws FileNotFoundException {
        File file = new File("cities.txt");
        Scanner inputFile = new Scanner(file);
        int index = 0;

        while (inputFile.hasNext() && index < namesOfCities.length) {
            namesOfCities[index] = inputFile.nextLine();
            cities[index] = new Vertex(namesOfCities[index]);
            System.out.println(" " + index + ".  " + namesOfCities[index]);
            index++;
        }

        numberOfCities = index;

        inputFile.close();
    }

    public void readFileWithDistances() throws FileNotFoundException {
        File file = new File("distances.txt");
        Scanner inputFile = new Scanner(file);
        int index = 0;
        String[] spDistance;

        int incr;

        while (inputFile.hasNext()) {
            distances[index] = inputFile.nextLine();
            spDistance = distances[index].split("[,]");
            int[] spNumberDistance = new int[spDistance.length];
            edges = new Edge[spDistance.length / 2];
            incr = 0;

            for (int i = 0; i < spDistance.length; i++) {
                spNumberDistance[i] = Integer.parseInt(spDistance[i]);
            }

            for (int i = 0; i < edges.length; i++) {
                edges[i] = new Edge(cities[spNumberDistance[i + incr + 1]], spNumberDistance[i + incr + 2]);
                incr++;
            }

            cities[spNumberDistance[0]].adjacencies = edges;
            index++;
        }

        numberOfDistances = index;

        inputFile.close();
    }

}
